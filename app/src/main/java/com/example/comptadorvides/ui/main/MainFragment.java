package com.example.comptadorvides.ui.main;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.comptadorvides.R;
import com.google.android.material.snackbar.Snackbar;

public class MainFragment extends Fragment {


    private ConstraintLayout main;
    private Button p1PoisonMore;
    private Button p1PoisonLess;
    private Button p2PoisonLess;
    private Button p2PoisonMore;
    private ImageButton p1LifeMore;
    private ImageButton p2LifeMore;
    private ImageButton p1LifeLess;
    private ImageButton p2LifeLess;
    private ImageButton lifeTwoToOne;
    private ImageButton lifeOneToTwo;
    private TextView counter1;
    private TextView counter2;

    private int life1;
    private int life2;
    private int poison1;
    private int poison2;

    private MainViewModel mViewModel;
    private View view;

    public static MainFragment newInstance() {
        return new MainFragment();
    }




    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.main_fragment, container, false);


        main = (ConstraintLayout) view.findViewById(R.id.main);
        p1PoisonMore = (Button) view.findViewById(R.id.p1PoisonMore);
        p1PoisonLess = (Button) view.findViewById(R.id.p1PoisonLess);
        p2PoisonLess = (Button) view.findViewById(R.id.p2PoisonLess);
        p2PoisonMore = (Button) view.findViewById(R.id.p2PoisonMore);
        p1LifeMore = (ImageButton) view.findViewById(R.id.p1LifeMore);
        p2LifeMore = (ImageButton) view.findViewById(R.id.p2LifeMore);
        p1LifeLess = (ImageButton) view.findViewById(R.id.p1LifeLess);
        p2LifeLess = (ImageButton) view.findViewById(R.id.p2LifeLess);
        lifeTwoToOne = (ImageButton) view.findViewById(R.id.lifeTwoToOne);
        lifeOneToTwo = (ImageButton) view.findViewById(R.id.lifeOneToTwo);
        counter1 = (TextView) view.findViewById(R.id.counter1);
        counter2 = (TextView) view.findViewById(R.id.counter2);


        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()) {
                    case R.id.lifeOneToTwo:
                        life1--;
                        life2++;
                        break;
                    case R.id.lifeTwoToOne:
                        life1++;
                        life2--;
                        break;
                    case R.id.p1LifeLess:
                        life1--;
                        break;
                    case R.id.p1LifeMore:
                        life1++;
                        break;
                    case R.id.p1PoisonLess:
                        poison1--;
                        break;
                    case R.id.p1PoisonMore:
                        poison1++;
                        break;
                    case R.id.p2LifeLess:
                        life2--;
                        break;
                    case R.id.p2LifeMore:
                        life2++;
                        break;
                    case R.id.p2PoisonLess:
                        poison2--;
                        break;
                    case R.id.p2PoisonMore:
                        poison2++;
                        break;




                }
                updateView();
            }
        };


        reset();

        p1PoisonMore.setOnClickListener(listener);
        p1PoisonLess.setOnClickListener(listener);
        p2PoisonLess.setOnClickListener(listener);
        p2PoisonMore.setOnClickListener(listener);
        p1LifeMore.setOnClickListener(listener);
        p2LifeMore.setOnClickListener(listener);
        p1LifeLess.setOnClickListener(listener);
        lifeTwoToOne.setOnClickListener(listener);
        lifeOneToTwo.setOnClickListener(listener);
        counter1.setOnClickListener(listener);
        counter2.setOnClickListener(listener);

        if (savedInstanceState != null) {
            life1 = savedInstanceState.getInt("life1"); // Extraiem les dades
            life2 = savedInstanceState.getInt("life2");
            poison1 = savedInstanceState.getInt("poison1");
            poison2 = savedInstanceState.getInt("poison2");

            updateView(); // Actualitzem els comptadors
        }


        return view;



    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.menu_main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();


        if (id == R.id.reset) {
            reset();
            Snackbar.make(view, "New Game!", Snackbar.LENGTH_LONG).show();
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt("life1", life1);
        outState.putInt("life2", life2);
        outState.putInt("poison1", poison1);
        outState.putInt("poison2", poison2);
    }


    private void reset() {
        poison1 = 0;
        poison2 = 0;
        life1 = 20;
        life2 = 20;

        updateView();

    }

    private void updateView() {
        counter1.setText(String.format("%d/%d", life1, poison1));
        counter2.setText(String.format("%d/%d", life2, poison2));
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(MainViewModel.class);
        // TODO: Use the ViewModel
    }

}